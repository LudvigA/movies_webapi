﻿using Assignment3_webAPI.Models.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Assignment3_webAPI.Models.DTO.FranchiseDTO
{
    public class FranchiseReadDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Desctiption { get; set; }

        public List<int> Movies { get; set; }
    }
}
