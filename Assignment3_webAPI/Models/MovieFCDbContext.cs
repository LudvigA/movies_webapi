﻿using Assignment3_webAPI.Models.Domain;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading.Tasks;

namespace Assignment3_webAPI.Models
{
    public class MovieFCDbContext : DbContext
    {
        public DbSet<Movie> Movies { get; set; }
        public DbSet<Franchise> Franchises { get; set; }
        public DbSet<Character> Characters { get; set; }

        public MovieFCDbContext([NotNullAttribute] DbContextOptions options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Franchise>().HasData(new Franchise() { Id = 1, Name = "LeaftForDeadFranchise", Desctiption = "Franchise with the Leaft4Dead movies" });

            modelBuilder.Entity<Movie>().HasData(new Movie() { Id = 1, MovieTitle = "Left4Dead", Genre = "Horror", ReleaseYear = new DateTime(1986, 08, 21), Director = "Jon Son", Picture = "https://th.bing.com/th/id/OIP.KSzt1w0LSbFufWUUUMJNBQHaEo?pid=ImgDet&rs=1", Trailer = "https://www.youtube.com/watch?v=quI772XuCpo", FranchiseId = 1 });
            modelBuilder.Entity<Movie>().HasData(new Movie() { Id = 2, MovieTitle = "Left4Dead2", Genre = "Horror", ReleaseYear = new DateTime(1988, 08, 21), Director = "Jon Son", Picture = "https://th.bing.com/th/id/OIP.KSzt1w0LSbFufWUUUMJNBQHaEo?pid=ImgDet&rs=1", Trailer = "https://www.youtube.com/watch?v=quI772XuCpo", FranchiseId = 1 });
            modelBuilder.Entity<Movie>().HasData(new Movie() { Id = 3, MovieTitle = "Something", Genre = "Drama", ReleaseYear = new DateTime(2000, 08, 21), Director = "Jon Son", Picture = "https://th.bing.com/th/id/OIP.KSzt1w0LSbFufWUUUMJNBQHaEo?pid=ImgDet&rs=1", Trailer = "https://www.youtube.com/watch?v=quI772XuCpo" });

            modelBuilder.Entity<Character>().HasData(new Character() { Id = 1, FullName = "John McSmith", Alias = "Beater of things", Gender = "Male", Picture = "https://th.bing.com/th/id/OIP.k4RNRBWgmiOTrRSWOcuGUAHaOW?pid=ImgDet&rs=1" });
            modelBuilder.Entity<Character>().HasData(new Character() { Id = 2, FullName = "John Wick", Alias = "Bobayoga", Gender = "Male", Picture = "https://th.bing.com/th/id/R.663c55023d9eb5f297d6ef19367f1890?rik=PjWhZloo1qCopA&pid=ImgRaw&r=0" });

            modelBuilder.Entity<Movie>()
                .HasMany(p => p.Characters)
                .WithMany(m => m.Movies)
                .UsingEntity<Dictionary<string, object>>(
                    "MovieCharacter",
                    r => r.HasOne<Character>().WithMany().HasForeignKey("CharacterId"),
                    l => l.HasOne<Movie>().WithMany().HasForeignKey("MovieId"),
                    je =>
                    {
                        je.HasKey("MovieId", "CharacterId");
                        je.HasData(
                            new { CharacterId = 1, MovieId = 1 },
                            new { CharacterId = 2, MovieId = 2 }
                        );
                    });

        }
    }
}
