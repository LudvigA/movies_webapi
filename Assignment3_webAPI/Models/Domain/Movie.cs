﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Assignment3_webAPI.Models.Domain
{
    [Table("Movie")]
    public class Movie
    {
        public int Id { get; set; }
        [Required]
        [MaxLength(80)]
        public string MovieTitle { get; set; }
        public string Genre { get; set; }
        public DateTime ReleaseYear { get; set; }
        [MaxLength(50)]
        public string Director { get; set; }
        public string Picture { get; set; }
        public string Trailer { get; set; }
        // Relationships with characters

        public ICollection<Character> Characters { get; set; }
        // Relationships with franchise
        public int? FranchiseId { get; set; }
        public Franchise? Franchise { get; set; }

    }
}
