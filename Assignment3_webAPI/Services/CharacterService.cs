﻿using Assignment3_webAPI.Models;
using Assignment3_webAPI.Models.Domain;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Assignment3_webAPI.Services
{
    public class CharacterService : ICharacterService
    {
        private readonly MovieFCDbContext _context;
        public CharacterService(MovieFCDbContext context)
        {
            _context = context;
        }
        public bool CharacterExists(int id)
        {
            return _context.Characters.Any(e => e.Id == id);
        }

        public async Task DeleteCharacterAsync(int id)
        {
            var character = await _context.Characters.FindAsync(id);
            _context.Characters.Remove(character);
            await _context.SaveChangesAsync();

        }

        public async Task<Character> GetCharacterAsync(int id)
        {
            return await _context.Characters.Include(c => c.Movies).Where(c => c.Id == id).FirstAsync();
        }

        public async Task<IEnumerable<Character>> GetCharactersAsync()
        {
            return await _context.Characters.Include(c => c.Movies).ToListAsync();
        }

        public async Task PostCharacerAsync(Character character)
        {
            _context.Characters.Add(character);
            await _context.SaveChangesAsync();
        }

        public async Task PutCharacterAsync(Character character)
        {
            _context.Entry(character).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }
    }
}
