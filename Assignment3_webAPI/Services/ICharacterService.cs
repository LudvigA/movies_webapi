﻿using Assignment3_webAPI.Models.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Assignment3_webAPI.Services
{
    public interface ICharacterService
    {
        public Task<IEnumerable<Character>> GetCharactersAsync();
        public Task<Character> GetCharacterAsync(int id);
        public Task PutCharacterAsync(Character character);
        public Task PostCharacerAsync(Character character);
        public Task DeleteCharacterAsync(int id);
        public bool CharacterExists(int id);
    }
}
