﻿using Assignment3_webAPI.Models;
using Assignment3_webAPI.Models.Domain;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Assignment3_webAPI.Services
{
    public class FranchiseService : IFranchiseService
    {
        private readonly MovieFCDbContext _context;
        public FranchiseService(MovieFCDbContext context)
        {
            _context = context;
        }
        public async Task DeleteFranchise(int id)
        {
            var franchise = await _context.Franchises.FindAsync(id);
            _context.Franchises.Remove(franchise);
            await _context.SaveChangesAsync();
        }

        public bool FranchiseExists(int id)
        {
            return _context.Franchises.Any(e => e.Id == id);
        }

        public async Task<IEnumerable<Character>> GetCharacterInFranchiseAsync(int id)
        {
            return await _context.Characters.Where(s => s.Movies.Any(c => c.FranchiseId == id)).Include(c => c.Movies).ToListAsync();
        }

        public async Task<Franchise> GetFranchiseAsync(int id)
        {
            return await _context.Franchises.Include(f => f.Movies).Where(f => f.Id == id).FirstAsync();
        }

        public async Task<IEnumerable<Movie>> GetFranchiseMoviesAsync(int id)
        {
            return await _context.Movies.Where(m => m.FranchiseId == id).Include(m => m.Characters).ToListAsync();
        }

        public async Task<IEnumerable<Franchise>> GetFranchisesAsync()
        {
            return await _context.Franchises.Include(f => f.Movies).ToListAsync();
        }

        public async Task PostFranchise(Franchise franchise)
        {
            _context.Franchises.Add(franchise);
            await _context.SaveChangesAsync();
        }

        public async Task PutFranchise(Franchise franchise)
        {
            _context.Entry(franchise).State = EntityState.Modified;
            await _context.SaveChangesAsync();

        }

        public async Task UpdateFranchiseMovies(int id, int[] movies)
        {
            Franchise franchiseToUpdateMovie = await _context.Franchises
                .Include(c => c.Movies)
                .Where(c => c.Id == id)
                .FirstAsync();

            // Loop through characters, try and assign to movies
            List<Movie> mov = new();
            foreach (int movId in movies)
            {
                Movie movie = await _context.Movies.FindAsync(movId);
                if (movie == null)
                    throw new KeyNotFoundException();
                mov.Add(movie);
            }
            franchiseToUpdateMovie.Movies = mov;
            await _context.SaveChangesAsync();
        }
    }
}
