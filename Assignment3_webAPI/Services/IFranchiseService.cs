﻿using Assignment3_webAPI.Models.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Assignment3_webAPI.Services
{
    public interface IFranchiseService
    {
        public Task<IEnumerable<Franchise>> GetFranchisesAsync();
        public Task<Franchise> GetFranchiseAsync(int id);
        public Task<IEnumerable<Movie>> GetFranchiseMoviesAsync(int id);
        public Task<IEnumerable<Character>> GetCharacterInFranchiseAsync(int id);
        public Task PutFranchise(Franchise franchise);
        public Task UpdateFranchiseMovies(int id, int[] movies);
        public Task PostFranchise(Franchise franchise);
        public Task DeleteFranchise(int id);
        public bool FranchiseExists(int id);
    }
}
