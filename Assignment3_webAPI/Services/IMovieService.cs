﻿using Assignment3_webAPI.Models.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Assignment3_webAPI.Services
{
    public interface IMovieService
    {
        public Task<IEnumerable<Movie>> GetMoviesAsync();
        public Task<Movie> GetMovieAsync(int id);
        public Task<IEnumerable<Character>> GetCharacterInMovieAsync(int id);
        public Task PostMovieAsync(Movie movie);
        public Task PutMovieAsync(Movie movie);
        public Task UpdateMovieCharatersAsync(int id, int[] characters);
        public Task DeleteMovieAsync(int id);
        public bool MovieExists(int id);
    }
}
