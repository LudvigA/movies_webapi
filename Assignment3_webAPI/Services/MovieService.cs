﻿using Assignment3_webAPI.Models;
using Assignment3_webAPI.Models.Domain;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Assignment3_webAPI.Services
{
    public class MovieService : IMovieService
    {
        private readonly MovieFCDbContext _context;
        public MovieService(MovieFCDbContext context)
        {
            _context = context;
        }
        public async Task DeleteMovieAsync(int id)
        {
            var movie = await _context.Movies.FindAsync(id);
            _context.Movies.Remove(movie);
            await _context.SaveChangesAsync();
        }

        public async Task<IEnumerable<Character>> GetCharacterInMovieAsync(int id)
        {
            return await _context.Characters.Where(s => s.Movies.Any(c => c.Id == id)).Include(c => c.Movies).ToListAsync();
        }

        public async Task<Movie> GetMovieAsync(int id)
        {
            return await _context.Movies.Include(m => m.Characters).Where(m => m.Id == id).FirstAsync();
        }

        public async Task<IEnumerable<Movie>> GetMoviesAsync()
        {
            return await _context.Movies.Include(m => m.Characters).ToListAsync();
        }

        public bool MovieExists(int id)
        {
            return _context.Movies.Any(e => e.Id == id);
        }

        public async Task PostMovieAsync(Movie movie)
        {
            _context.Movies.Add(movie);
            await _context.SaveChangesAsync();
        }

        public async Task PutMovieAsync(Movie movie)
        {
            _context.Entry(movie).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }

        public async Task UpdateMovieCharatersAsync(int id, int[] characters)
        {
            Movie movieToUpdateCharacter = await _context.Movies
              .Include(c => c.Characters)
              .Where(c => c.Id == id)
              .FirstAsync();

            // Loop through characters, try and assign to movies

            List<Character> chars = new();
            foreach (int charId in characters)
            {
                Character character = await _context.Characters.FindAsync(charId);
                if (character == null)
                    throw new KeyNotFoundException();
                chars.Add(character);
            }
            movieToUpdateCharacter.Characters = chars;
            await _context.SaveChangesAsync();
        }
    }
}
