﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Assignment3_webAPI.Models.Domain;
using Assignment3_webAPI.Models.DTO.FranchiseDTO;
using AutoMapper;

namespace Assignment3_webAPI.Profiles
{
    public class FranchiseProfile : Profile
    {
        public FranchiseProfile()
        {
            //Franchise - FranchiseReadDTO - Turning related movies to Array
            CreateMap<Franchise, FranchiseReadDTO>().ForMember(fdto => fdto.Movies, opt => opt.MapFrom(f => f.Movies.Select(m => m.Id).ToArray())).ReverseMap();

            //Franchise - FranchiseCreateDTO
            CreateMap<Franchise, FranchiseCreateDTO>().ReverseMap();

            //Franchise - FranchiseUpdateDTO
            CreateMap<Franchise, FranchiseUpdateDTO>().ReverseMap();
        }
    }
}
