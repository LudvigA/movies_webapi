﻿using Assignment3_webAPI.Models.Domain;
using Assignment3_webAPI.Models.DTO.CharacterDTO;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Assignment3_webAPI.Profiles
{
    public class CharacterProfile : Profile
    {
        public CharacterProfile()
        {
            //Character - CharacterReadDTO
            CreateMap<Character, CharacterReadDTO>().ForMember(cdto => cdto.Movies, opt => opt.MapFrom(c => c.Movies.Select(m => m.Id).ToArray())).ReverseMap();

            //Character - CharacterCreateDTO
            CreateMap<Character, CharacterCreateDTO>().ReverseMap();

            //Character - CharacterUpdateDTO
            CreateMap<Character, CharacterUpdateDTO>().ReverseMap();

        }
    }
}
