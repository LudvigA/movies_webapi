﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Assignment3_webAPI.Models.Domain;
using Assignment3_webAPI.Models.DTO.MovieDTO;
using AutoMapper;

namespace Assignment3_webAPI.Profiles
{
    public class MovieProfile : Profile
    {
        public MovieProfile()
        {
            //Movie - MovieReadDTO
            CreateMap<Movie, MovieReadDTO>().ForMember(mdto => mdto.Characters, opt => opt.MapFrom(m => m.Characters.Select(c => c.Id).ToArray())).ReverseMap();

            //Movie - MovieUpdateDTO
            CreateMap<Movie, MovieUpdateDTO>().ReverseMap();

            //Movie - MovieCreateDTO
            CreateMap<Movie, MovieCreateDTO>().ReverseMap();
        }
    }
}
