﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Assignment3_webAPI.Models;
using Assignment3_webAPI.Models.Domain;
using System.Net.Mime;
using AutoMapper;
using Assignment3_webAPI.Models.DTO.FranchiseDTO;
using Assignment3_webAPI.Models.DTO.MovieDTO;
using Assignment3_webAPI.Models.DTO.CharacterDTO;
using Assignment3_webAPI.Services;

namespace Assignment3_webAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class FranchisesController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IFranchiseService _franchiseService;

        public FranchisesController(IMapper mapper, IFranchiseService franchiseService)
        {
            _mapper = mapper;
            _franchiseService = franchiseService;
        }

        /// <summary>
        /// Gets all franchises.
        /// </summary>
        /// <returns>Returns a list of all franchises</returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<FranchiseReadDTO>>> GetFranchises()
        {

            return _mapper.Map<List<FranchiseReadDTO>>(await _franchiseService.GetFranchisesAsync());
        }

        /// <summary>
        /// Gets a franchise based on given Id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Returns a franchise if success, 404 Not Found if failed</returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<FranchiseReadDTO>> GetFranchise(int id)
        {
            var franchise = await _franchiseService.GetFranchiseAsync(id);

            if (franchise == null)
            {
                return NotFound();
            }

            return _mapper.Map<FranchiseReadDTO>(franchise);
        }


        /// <summary>
        /// Gets all movies in a franchise based on Id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Returns a list of movies</returns>
        [HttpGet("GetFranchiseMovies/{id}")]
        public async Task<IEnumerable<MovieReadDTO>> GetFranchiseMovies(int id)
        {

            var movies = await _franchiseService.GetFranchiseMoviesAsync(id);
            return _mapper.Map<List<MovieReadDTO>>(movies);
        }


        /// <summary>
        /// Gets all characters in a franchise based on Id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Returns a list of characters</returns>
        [HttpGet("GetCharacterInFranchise/{id}")]
        public async Task<IEnumerable<CharacterReadDTO>> GetCharacterInFranchise(int id)
        {
            var characters = await _franchiseService.GetCharacterInFranchiseAsync(id);
            return _mapper.Map<List<CharacterReadDTO>>(characters);
        }

        /// <summary>
        /// Updates a franchise based on given Id.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="franchisedto"></param>
        /// <returns>Returns 204 No content if successful, 404 Not Found or 400 Bad Request if it fails</returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> PutFranchise(int id, FranchiseUpdateDTO franchisedto)
        {
            if (id != franchisedto.Id)
            {
                return BadRequest();
            }
            if (!_franchiseService.FranchiseExists(id))
            {
                return NotFound();
            }
            Franchise domainFranchise = _mapper.Map<Franchise>(franchisedto);
            await _franchiseService.PutFranchise(domainFranchise);
            return NoContent();
        }

        /// <summary>
        /// Updates movies in a franchise based on franchiseId and list of MovieId's
        /// </summary>
        /// <param name="id"></param>
        /// <param name="movies"></param>
        /// <returns>Returns 204 No content if successful, 404 Not Found or 400 Bad Request if it fails</returns>
        [HttpPut("{id}/movies")]
        public async Task<IActionResult> UpdateFranchiseMovies(int id, int[] movies)
        {
            if (!_franchiseService.FranchiseExists(id))
            {
                return NotFound();
            }
            await _franchiseService.UpdateFranchiseMovies(id, movies);
            return NoContent();
        }


        /// <summary>
        /// Creates a new franchise.
        /// </summary>
        /// <param name="franchisedto"></param>
        /// <returns>Returns 201 Created franchise on Sucess, 400 Bad Request if it fails</returns>
        [HttpPost]
        public async Task<ActionResult<Franchise>> PostFranchise(FranchiseCreateDTO franchisedto)
        {
            Franchise domainFranchise = _mapper.Map<Franchise>(franchisedto);
            await _franchiseService.PostFranchise(domainFranchise);
            return CreatedAtAction("GetFranchise", new { id = domainFranchise.Id }, domainFranchise);
        }

        /// <summary>
        /// Deletes a franchise based on given Id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Returns 204 No Content on success, 404 Not if it fails</returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteFranchise(int id)
        {
            if (!_franchiseService.FranchiseExists(id))
            {
                return NotFound();
            }
            Franchise franchise = await _franchiseService.GetFranchiseAsync(id);
            franchise.Movies.Select(m => m.FranchiseId = null);
            await _franchiseService.DeleteFranchise(id);

            return NoContent();
        }
    }
}
