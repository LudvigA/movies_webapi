﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Assignment3_webAPI.Models;
using Assignment3_webAPI.Models.Domain;
using System.Net.Mime;
using AutoMapper;
using Assignment3_webAPI.Models.DTO.CharacterDTO;
using Assignment3_webAPI.Services;

namespace Assignment3_webAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class CharactersController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly ICharacterService _characterService;

        public CharactersController(IMapper mapper, ICharacterService characterService)
        {
            _mapper = mapper;
            _characterService = characterService;
        }

        /// <summary>
        /// Gets all characters.
        /// </summary>
        /// <returns>Returns a list of all characters</returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CharacterReadDTO>>> GetCharacters()
        {
            return _mapper.Map<List<CharacterReadDTO>>(await _characterService.GetCharactersAsync());
        }

        /// <summary>
        /// Gets a character based on given Id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Returns a character if success, 404 Not Found if failed</returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<CharacterReadDTO>> GetCharacter(int id)
        {
            var character = await _characterService.GetCharacterAsync(id);

            if (character == null)
            {
                return NotFound();
            }

            return _mapper.Map<CharacterReadDTO>(character);
        }

        /// <summary>
        /// Updates a character based on given Id.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="characterdto"></param>
        /// <returns>Returns 204 No content if successful, 404 Not Found or 400 Bad Request if it fails</returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCharacter(int id, CharacterUpdateDTO characterdto)
        {
            if (id != characterdto.Id)
            {
                return BadRequest();
            }
            if (!_characterService.CharacterExists(id))
            {
                return NotFound();
            }
            Character domainCharacter = _mapper.Map<Character>(characterdto);
            await _characterService.PutCharacterAsync(domainCharacter);
            return NoContent();
        }

        /// <summary>
        /// Creates a character.
        /// </summary>
        /// <param name="characterdto"></param>
        /// <returns>Returns 201 Created movie on Sucess, 400 Bad Request if it fails</returns>
        [HttpPost]
        public async Task<ActionResult<Character>> PostCharacter(CharacterCreateDTO characterdto)
        {
            Character domainCharacter = _mapper.Map<Character>(characterdto);
            await _characterService.PostCharacerAsync(domainCharacter);
            return CreatedAtAction("GetCharacter", new { id = domainCharacter.Id }, _mapper.Map<CharacterReadDTO>(domainCharacter));

        }

        /// <summary>
        /// Deletes a character based on given Id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Returns 204 No Content on success, 404 Not if it fails</returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteCharacter(int id)
        {
            if (!_characterService.CharacterExists(id))
            {
                return NotFound();
            }
            await _characterService.DeleteCharacterAsync(id);
            return NoContent();
        }
    }
}
