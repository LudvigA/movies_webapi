﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Assignment3_webAPI.Models;
using Assignment3_webAPI.Models.Domain;
using System.Net.Mime;
using Microsoft.EntityFrameworkCore;
using AutoMapper;
using Assignment3_webAPI.Models.DTO.MovieDTO;
using Assignment3_webAPI.Models.DTO.CharacterDTO;
using Assignment3_webAPI.Services;

namespace Assignment3_webAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class MoviesController : ControllerBase
    {
        private readonly IMovieService _movieService;
        private readonly IMapper _mapper;

        public MoviesController(IMapper mapper, IMovieService movieService)
        {
            _mapper = mapper;
            _movieService = movieService;
        }

        /// <summary>
        /// Gets all movies.
        /// </summary>
        /// <returns>Returns a list of all movies</returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<MovieReadDTO>>> GetMovies()
        {
            return _mapper.Map<List<MovieReadDTO>>(await _movieService.GetMoviesAsync());
        }

        /// <summary>
        /// Gets movie based on given Id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Returns a movie if success, 404 Not Found if failed</returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<MovieReadDTO>> GetMovie(int id)
        {
            var movie = await _movieService.GetMovieAsync(id);
            if (movie == null)
            {
                return NotFound();
            }

            return _mapper.Map<MovieReadDTO>(movie);
        }

        /// <summary>
        /// Gets all characters in a movie based on given Id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>A list of characters</returns>
        [HttpGet("GetCharacterInMovie/{id}")]
        public async Task<IEnumerable<CharacterReadDTO>> GetCharacterInMovie(int id)
        {
            var characters = await _movieService.GetCharacterInMovieAsync(id);
            return _mapper.Map<List<CharacterReadDTO>>(characters);

        }

        /// <summary>
        /// Updates a movie based on given Id.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="moviedto"></param>
        /// <returns>Returns 204 No content if successful, 404 Not Found or 400 Bad Request if it fails</returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> PutMovie(int id, MovieUpdateDTO moviedto)
        {
            if (id != moviedto.Id)
            {
                return BadRequest();
            }
            if (!_movieService.MovieExists(id))
            {
                return NotFound();
            }
            Movie domainMovie = _mapper.Map<Movie>(moviedto);
            await _movieService.PutMovieAsync(domainMovie);
            return NoContent();
        }

        /// <summary>
        /// Updates characters in a movie based on given MovieId and list of CharacterId's.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="characters"></param>
        /// <returns>Returns 204 No content if successful, 404 Not Found or 400 Bad Request if it fails</returns>
        [HttpPut("{id}/characters")]
        public async Task<IActionResult> UpdateMovieCharacters(int id, int[] characters)
        {
            if (!_movieService.MovieExists(id))
            {
                return NotFound();
            }
            await _movieService.UpdateMovieCharatersAsync(id, characters);
            return NoContent();
        }



        /// <summary>
        /// Creates a new movie.
        /// </summary>
        /// <param name="moviedto"></param>
        /// <returns>Returns 201 Created movie on Sucess, 400 Bad Request if it fails</returns>
        [HttpPost]
        public async Task<ActionResult<Movie>> PostMovie(MovieCreateDTO moviedto)
        {
            Movie domainMovie = _mapper.Map<Movie>(moviedto);
            await _movieService.PostMovieAsync(domainMovie);
            return CreatedAtAction("GetMovie", new { id = domainMovie.Id }, _mapper.Map<MovieReadDTO>(domainMovie));

        }

        /// <summary>
        /// Deletes a movie based on given Id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>>Returns 204 No Content on success, 404 Not if it fails</returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteMovie(int id)
        {
            if (!_movieService.MovieExists(id))
            {
                return NotFound();
            }
            await _movieService.DeleteMovieAsync(id);
            return NoContent();
        }
    }
}
