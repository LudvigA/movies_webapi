# Web API
This project consists of an API made with ASP.Net Core that interracts with a database made with Entity Framework Core.
The database conists of tables with Characters, Movies and Franchises which have different applied buisness rules.
## The Database
Buisness rules:

* A movie can contain many characters, and a character can star in multiple movies.
* A movie belongs to one franchise, but a franchise can contain many movies.

The database must be created and seeded with dummy data to be able to interract with the API.
You should be able to run the 'update-database' command in Package Manager Console with the already existing migrations.

## The API

The API has implemented Full CRUD for Moves,Characters and Franchises.
* Data Transer Objects(DTO) are implemented with AutoMapper
* Services to clean up controllers 
* SwaggerDocumentation 

## Contributers

* Ludvig Ånestad
* Stian Kvamme
